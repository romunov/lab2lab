Package `lab2lab` houses the same name function which will convert genotypes from one laboratory's coding into a reference laboratory's genotype codes.

Usage is simple. Import genotypes from a desired laboratory and use

```r
data(refs)

lab.from <- read.table("./data/gr.csv", header = TRUE, sep = ";")
lab2lab(genotypes = lab.from, translate.from = "gr", translate.to = "lj", 
        warn = FALSE, reftable = refs)
```

See `data(refs)` for laboratory designations and code tables used in each laboratory. If you have your own code table data.frame, pass it to `reftable` argument of the `lab2lab` function. To prevent any crashes, it's recommended that the structure matches `refs`. Your task is to add new genotypes and loci. You can export `refs` to a file (`write.table(lab2lab:::refs, file = "new_refs.csv", sep = ";")`), open it in a respectable spreadsheet program and append necessary information.

See `vignette("translate_gen")` for all the information, comments and TODOs.